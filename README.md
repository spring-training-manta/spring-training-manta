# MANTA Spring Framework Training

Praktická část: 
-----------------------------------------------------------------
Pushněte vaše řešení úlohy do Gitu do větve s názvem jmenoprijmeni
Časová dotace 1MD


Vyviňte jednoduchou (ale robustní) webovou aplikaci na poskytování informací o počasí. Aplikace uživateli na dotaz vrátí informaci o počasí.
Aplikace bude vystavovat jednu REST službu, která zpracovává požadavky na informaci o počasí a vrací response.
Dotaz (request) bude obsahovat informaci: místo, datum, čas
Odpověď (response) systému bude: místo, datum, čas, teplota, oblačnost, síla a směr větru

Použijte ExceptionHandler vracející strukturovanou chybu pro ošetření nevalidního requestu (např datum předpovědi).
Vytvořte 3 komponenty pro poskytování informací o počasí.
  1) RandomWeatherProvider: Defaultní implementace. Bude vracet náhodně generovaná data (nemusí odpovídat realitě) - používá se v produkčním kódu.
  2) DatabaseWeatherProvider: Bude vracet data z databáze z tabulky předpovědí počasí - inicializuje se jen při aktivaci konkrétního Spring profilu. Bude přistupovat k DB za použití JdbcTemplate nebo pomocí JPA.
  3) StupWeatherProvider: Bude vracet vždy konstantní data o počasí (natvrdo) - používá se v testovacím kontextu.

Aplikace bude nasazená na aplikačním serveru (např. Tomcat, Jetty), napsaná v Javě a Spring MVC nebo Spring Boot.
Nevytvářejte view vrstvu ani front-end. Nebude na to čas.
Vytvořte aspekt (AOP), který bude logovat všechny příchozí requesty.
Logování realizujte pomocí logovací knihovny (ne system out print)
Pro konfiguraci použijte property soubor umístěný v resources (např. konfigurace JDBC připojení)
Aplikaci vhodně strukturujte do balíčků a vrstev.

Testy aplikace
	Java kód by měl být pokryt testy minimálně ze 70%.
	Napište alespoň 1 integrační test (startující testovací Spring context).
	Integrační test bude používat testovací implementaci beany (StubWeatherProvider).
	Použijte alespoň jednou parametrizovaný test (jUnit 5).
	Test DB vrstvy musí být transakční. Ppo proběhnutí testu se provede rollback, i když v aplikaci nepoužíváte žádné modifikace dat - insert, update, delete, merge, ...

Zdrojáky budou buildovány pomocí Maven.